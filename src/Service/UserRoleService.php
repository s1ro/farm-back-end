<?php


namespace App\Service;


use App\Entity\User;

class UserRoleService
{
    /**
     * @return array
     */
    public static function availableRoles(): array
    {
        return [User::ROLE_ADMIN, User::ROLE_EXPERT, User::ROLE_MODERATOR];
    }
}