<?php


namespace App\GraphQl\Type;


use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type(name="Token")
 * Class TokenType
 * @package App\GraphQl\Type
 */
class TokenType
{
    /**
     * @var string
     */
    private $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @Field()
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}