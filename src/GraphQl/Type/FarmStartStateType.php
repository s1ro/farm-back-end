<?php


namespace App\GraphQl\Type;


use App\Entity\Environment;
use App\Entity\GameObjectsDTO\AnimalDTO;
use App\Entity\GameObjectsDTO\DynamicObjectDTO;
use App\Entity\GameObjectsDTO\StaticObjectDTO;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type(name="FarmStartState")
 * Class FarmStartStateType
 * @package App\GraphQl\Type
 */
class FarmStartStateType
{
    /**
     * @var StaticObjectDTO[]
     */
    private $staticObjects;

    /**
     * @var DynamicObjectDTO[]
     */
    private $dynamicObjects;

    /**
     * @var AnimalDTO[]
     */
    private $animals;

    /**
     * @var Environment[]
     */
    private $environments;

    public function __construct(array $staticObjects, array $dynamicObjects, array $animals, array $environments)
    {
        $this->staticObjects = $staticObjects;
        $this->dynamicObjects = $dynamicObjects;
        $this->animals = $animals;
        $this->environments = $environments;
    }

    /**
     * @Field()
     * @return StaticObjectDTO[]
     */
    public function getStaticObjects(): array
    {
        return $this->staticObjects;
    }

    /**
     * @Field()
     * @return DynamicObjectDTO[]
     */
    public function getDynamicObjects(): array
    {
        return $this->dynamicObjects;
    }

    /**
     * @Field()
     * @return AnimalDTO[]
     */
    public function getAnimals(): array
    {
        return $this->animals;
    }

    /**
     * @Field()
     * @return Environment[]
     */
    public function getEnvironments(): array
    {
        return $this->environments;
    }
}