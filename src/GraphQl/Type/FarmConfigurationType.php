<?php


namespace App\GraphQl\Type;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type(name="FarmConfiguration")
 * Class FarmConfigurationType
 * @package App\GraphQl\Type
 */
class FarmConfigurationType
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $staticObjectConfiguration;

    /**
     * @var string
     */
    private $dynamicObjectConfiguration;

    /**
     * @var string
     */
    private $animalConfiguration;

    /**
     * FarmConfigurationType constructor.
     * @param int $id
     * @param string $staticObjectConfiguration
     * @param string $dynamicObjectConfiguration
     * @param string $animalConfiguration
     */
    public function __construct(int $id, string $staticObjectConfiguration, string $dynamicObjectConfiguration, string $animalConfiguration)
    {
        $this->id = $id;
        $this->staticObjectConfiguration = $staticObjectConfiguration;
        $this->dynamicObjectConfiguration = $dynamicObjectConfiguration;
        $this->animalConfiguration = $animalConfiguration;
    }

    /**
     * @Field()
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @Field()
     * @return string
     */
    public function getStaticObjectConfiguration(): string
    {
        return $this->staticObjectConfiguration;
    }

    /**
     * @Field()
     * @return string
     */
    public function getDynamicObjectConfiguration(): string
    {
        return $this->dynamicObjectConfiguration;
    }

    /**
     * @Field()
     * @return string
     */
    public function getAnimalConfiguration(): string
    {
        return $this->animalConfiguration;
    }


}