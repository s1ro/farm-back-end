<?php


namespace App\GraphQl\Type;


use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type(name="User")
 * Class UserType
 * @package App\GraphQl\Type
 */
class UserType
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string[]
     */
    private $roles;

    public function __construct(
        int $id,
        string $email,
        ?string $name,
        array $roles
    )
    {
        $this->id = $id;
        $this->email = $email;
        $this->name = $name;
        $this->roles = $roles;
    }

    /**
     * @Field()
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @Field()
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @Field()
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @Field()
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}