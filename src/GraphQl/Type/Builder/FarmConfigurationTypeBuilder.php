<?php


namespace App\GraphQl\Type\Builder;


use App\Entity\FarmStartState;
use App\GraphQl\Type\FarmConfigurationType;
use App\GraphQl\Type\FarmStartStateType;

class FarmConfigurationTypeBuilder
{
    /**
     * @param FarmStartState $farmStartState
     * @return FarmConfigurationType
     */
    public static function build(FarmStartState $farmStartState): FarmConfigurationType
    {
        return new FarmConfigurationType(
            $farmStartState->getId(),
            json_encode($farmStartState->getStaticObjectConfig()),
            json_encode($farmStartState->getDynamicObjectConfig()),
            json_encode($farmStartState->getAnimalConfig())
        );
    }

    /**
     * @param FarmStartState[] $farmStartStates
     * @return FarmConfigurationType[]
     */
    public static function batchBuild(array $farmStartStates): array
    {
        $result = [];
        foreach ($farmStartStates as $farmStartState) {
            $result[] = self::build($farmStartState);
        }

        return $result;
    }
}