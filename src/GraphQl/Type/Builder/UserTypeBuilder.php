<?php


namespace App\GraphQl\Type\Builder;


use App\Entity\User;
use App\GraphQl\Type\UserType;

class UserTypeBuilder
{
    /**
     * @param User $user
     * @return UserType
     */
    public static function build(User $user): UserType
    {
        return new UserType(
            $user->getId(),
            $user->getEmail(),
            $user->getEmail(),
            $user->getRoles()
        );
    }
}