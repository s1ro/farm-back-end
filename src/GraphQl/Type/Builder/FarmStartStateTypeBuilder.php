<?php


namespace App\GraphQl\Type\Builder;


use App\Entity\Game;
use App\GraphQl\Type\FarmStartStateType;

class FarmStartStateTypeBuilder
{
    /**
     * @param Game $game
     * @return FarmStartStateType
     */
    public static function build(Game $game): FarmStartStateType
    {
        return new FarmStartStateType(
            $game->getStaticObjects(),
            $game->getDynamicObjects(),
            $game->getAnimals(),
            []
        );
    }
}