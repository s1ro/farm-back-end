<?php


namespace App\GraphQl\Exception;


use TheCodingMachine\GraphQLite\Exceptions\GraphQLException;

class GraphQlAuthException extends GraphQLException
{
    /**
     * @return static
     */
    public static function createUserNotFound(): self
    {
        return new self('User not found', 404, null, 'user.not_found');
    }
}