<?php


namespace App\GraphQl\Service\Generator;


use App\Entity\FarmStartState;
use App\Entity\Game;
use App\Entity\GameObject;
use App\Entity\ObjectSize;
use App\Repository\GameObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

class GameObjectGenerator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManger;

    /**
     * @var GameObjectRepository
     */
    private $gameObjectRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        GameObjectRepository $gameObjectRepository
    )
    {
        $this->entityManger = $entityManager;
        $this->gameObjectRepository = $gameObjectRepository;
    }

    /**
     * @param FarmStartState $farmStartState
     * @param Game $game
     */
    public function generate(FarmStartState $farmStartState, Game $game): void
    {
        foreach ($farmStartState->getStaticObjectConfig() as $staticObjectConfig) {
            $object = $this->gameObjectRepository->findOneBy(['type' => $staticObjectConfig['type'], 'game' => null]);
            if (!$object) {
                continue;
            }
            for ($i = 0; $i < (int)$staticObjectConfig['quantity']; $i++) {
                $size = new ObjectSize();
                $size
                    ->setXCoord($object->getSize()->getXCoord())
                    ->setYCoord($object->getSize()->getYCoord());
                $this->entityManger->persist($size);
                $this->entityManger->flush();
                $generatedObject = new GameObject();
                $generatedObject
                    ->setXCoord($object->getXCoord())
                    ->setYCoord($object->getYCoord())
                    ->setGame($game)
                    ->setSize($size)
                    ->setType($object->getType())
                    ->setObjectType($object->getObjectType())
                    ->setName($object->getName());
                $this->entityManger->persist($generatedObject);
            }
        }
        foreach ($farmStartState->getDynamicObjectConfig() as $dynamicObjectConfig) {
            $object = $this->gameObjectRepository->findOneBy(['type' => $dynamicObjectConfig['type'], 'game' => null]);
            if (!$object) {
                continue;
            }
            for ($i = 0; $i < (int)$dynamicObjectConfig['quantity']; $i++) {
                $size = new ObjectSize();
                $size
                    ->setXCoord($object->getSize()->getXCoord())
                    ->setYCoord($object->getSize()->getYCoord());
                $this->entityManger->persist($size);
                $this->entityManger->flush();
                $generatedObject = new GameObject();
                $generatedObject
                    ->setXCoord($object->getXCoord())
                    ->setYCoord($object->getYCoord())
                    ->setGame($game)
                    ->setSize($size)
                    ->setType($object->getType())
                    ->setObjectType($object->getObjectType())
                    ->setName($object->getName())
                    ->setObjectState($object->getObjectState());
                $this->entityManger->persist($generatedObject);
            }
        }
        foreach ($farmStartState->getAnimalConfig() as $animalConfig) {
            $object = $this->gameObjectRepository->findOneBy(['type' => $animalConfig['type'], 'game' => null]);
            if (!$object) {
                continue;
            }
            for ($i = 0; $i < (int)$animalConfig['quantity']; $i++) {
                $size = new ObjectSize();
                $size
                    ->setXCoord($object->getSize()->getXCoord())
                    ->setYCoord($object->getSize()->getYCoord());
                $this->entityManger->persist($size);
                $this->entityManger->flush();
                $generatedObject = new GameObject();
                $generatedObject
                    ->setXCoord($object->getXCoord())
                    ->setYCoord($object->getYCoord())
                    ->setGame($game)
                    ->setSize($size)
                    ->setType($object->getType())
                    ->setObjectType($object->getObjectType())
                    ->setName($object->getName())
                    ->setObjectState($object->getObjectState())
                    ->setHealthPoints(100)
                    ->setFoodPoints(100)
                    ->setWaterPoints(100);
                $this->entityManger->persist($generatedObject);
            }
        }
        $this->entityManger->flush();
    }
}