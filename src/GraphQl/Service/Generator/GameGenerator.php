<?php


namespace App\GraphQl\Service\Generator;


use App\Entity\FarmStartState;
use App\Entity\Game;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class GameGenerator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var GameObjectGenerator
     */
    private $gameObjectGenerator;

    public function __construct(EntityManagerInterface $entityManager, GameObjectGenerator $gameObjectGenerator)
    {
        $this->entityManager = $entityManager;
        $this->gameObjectGenerator = $gameObjectGenerator;
    }

    /**
     * @param User $user
     * @param FarmStartState $farmStartState
     * @return Game
     */
    public function generate(User $user, FarmStartState $farmStartState): Game
    {
        $game = new Game();
        $game
            ->setUser($user)
            ->setFarmStartState($farmStartState);
        $this->entityManager->persist($game);
        $this->entityManager->flush();
        $this->gameObjectGenerator->generate($farmStartState, $game);
        return $game;
    }
}