<?php


namespace App\GraphQl\Service;


use App\Entity\FarmStartState;
use App\Entity\Game;
use App\Entity\GameObject;
use App\Entity\ObjectSize;
use App\GraphQl\Service\Generator\GameGenerator;
use App\GraphQl\Type\Builder\FarmConfigurationTypeBuilder;
use App\GraphQl\Type\Builder\FarmStartStateTypeBuilder;
use App\GraphQl\Type\FarmConfigurationType;
use App\GraphQl\Type\FarmStartStateType;
use App\Repository\FarmStartStateRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class StartGameService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var FarmStartStateRepository
     */
    private $farmStartStateRepository;

    /**
     * @var GameGenerator
     */
    private $gameGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        FarmStartStateRepository $farmStartStateRepository,
        GameGenerator $gameGenerator
    )
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->farmStartStateRepository = $farmStartStateRepository;
        $this->gameGenerator = $gameGenerator;
    }

    /**
     * @return FarmConfigurationType[]
     */
    public function getFarmConfigurations(): array
    {
        $farmStartStates = $this->farmStartStateRepository->findAll();
        return FarmConfigurationTypeBuilder::batchBuild($farmStartStates);
    }

    /**
     * @param int $farmStartStateId
     * @return FarmStartStateType
     */
    public function startGame(int $farmStartStateId): FarmStartStateType
    {
        //TODO USER WILL BE PROVIDED
        $user = $this->userRepository->findOneBy(['email' => 'test-start@test.com']);

        $farmStartState = $this->farmStartStateRepository->findOneBy(['id' => $farmStartStateId]);
        $game = $this->gameGenerator->generate($user, $farmStartState);
        $this->entityManager->refresh($game);
        return FarmStartStateTypeBuilder::build($game);
    }
}