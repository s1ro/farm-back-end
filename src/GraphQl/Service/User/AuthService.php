<?php


namespace App\GraphQl\Service\User;


use App\Entity\User;
use App\Entity\UserToken;
use App\GraphQl\Exception\GraphQlAuthException;
use App\GraphQl\Type\TokenType;
use App\Repository\UserRepository;
use App\Repository\UserTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class AuthService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var UserProviderInterface
     */
    private $userProvider;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    /**
     * @var TokenProvider
     */
    private $tokenProvider;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        UserProviderInterface $userProvider,
        UserRepository $userRepository,
        UserTokenRepository $userTokenRepository,
        TokenProvider $tokenProvider
    )
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->userProvider = $userProvider;
        $this->userRepository = $userRepository;
        $this->userTokenRepository = $userTokenRepository;
        $this->tokenProvider = $tokenProvider;
    }

    /**
     * @param string $email
     * @param string $password
     * @return TokenType
     * @throws GraphQlAuthException
     */
    public function login(string $email, string $password): TokenType
    {
        try {
            /** @var User $user */
            $user = $this->userProvider->loadUserByUsername($email);
        } catch (UsernameNotFoundException $e) {
            throw GraphQlAuthException::createUserNotFound();
        }
        if (!$this->passwordEncoder->isPasswordValid($user, $password)) {
            throw GraphQlAuthException::createUserNotFound();
        }
        $userToken = new UserToken();
        $userToken
            ->setUser($user)
            ->setToken(Uuid::uuid4());
        $this->entityManager->persist($userToken);
        $this->entityManager->flush();

        return new TokenType($userToken->getToken());
    }

    /**
     * @param User $user
     * @return bool
     */
    public function logout(User $user): bool
    {
        $token = $this->tokenProvider->getToken();
        if (!$token) {
            return true;
        }
        $userToken = $this->userTokenRepository->findOneBy(['token' => $token, 'user' => $user]);
        if ($userToken !== null) {
            $this->entityManager->remove($userToken);
            $this->entityManager->flush();
        }
        return true;
    }
}