<?php


namespace App\GraphQl\Service\User;


use Symfony\Component\HttpFoundation\RequestStack;

class TokenProvider
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->requestStack->getCurrentRequest()->headers->get('X-AUTH-TOKEN');
    }
}