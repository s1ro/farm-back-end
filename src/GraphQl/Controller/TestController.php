<?php

namespace App\GraphQl\Controller;

use TheCodingMachine\GraphQLite\Annotations\Query;

class TestController
{
    /**
     * @Query()
     * @param string|null $message
     * @return string
     */
    public function test(?string $message = null): string
    {
        return $message ?? 'Hello World!';
    }
}