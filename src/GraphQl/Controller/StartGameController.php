<?php


namespace App\GraphQl\Controller;


use App\GraphQl\Service\StartGameService;
use App\GraphQl\Type\FarmConfigurationType;
use App\GraphQl\Type\FarmStartStateType;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;

class StartGameController
{
    /**
     * @var StartGameService
     */
    private $startGameService;

    public function __construct(StartGameService $startGameService)
    {
        $this->startGameService = $startGameService;
    }

    /**
     * @Query()
     * @return FarmConfigurationType[]
     */
    public function getFarmConfigurations(): array
    {
        return $this->startGameService->getFarmConfigurations();
    }

    /**
     * @Mutation()
     * @param int $farmConfigurationId
     * @return FarmStartStateType
     */
    public function startGame(int $farmConfigurationId): FarmStartStateType
    {
        return $this->startGameService->startGame($farmConfigurationId);
    }
}