<?php


namespace App\GraphQl\Controller\User;


use App\Entity\User;
use App\GraphQl\Exception\GraphQlAuthException;
use App\GraphQl\Service\User\AuthService;
use App\GraphQl\Type\TokenType;
use TheCodingMachine\GraphQLite\Annotations\InjectUser;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Mutation;

class AuthController
{
    /**
     * @var AuthService
     */
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @Mutation()
     * @param string $email
     * @param string $password
     * @return TokenType
     * @throws GraphQlAuthException
     */
    public function login(string $email, string $password): TokenType
    {
        return $this->authService->login($email, $password);
    }

    /**
     * @Mutation()
     * @Logged()
     * @InjectUser(for="$user")
     * @param User|null $user
     * @return bool
     */
    public function logout(?User $user): bool
    {
        if (!$user) {
            return true;
        }
        return $this->authService->logout($user);
    }
}