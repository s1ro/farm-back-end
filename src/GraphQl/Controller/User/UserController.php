<?php


namespace App\GraphQl\Controller\User;


use App\Entity\User;
use App\GraphQl\Type\Builder\UserTypeBuilder;
use App\GraphQl\Type\UserType;
use TheCodingMachine\GraphQLite\Annotations\InjectUser;
use TheCodingMachine\GraphQLite\Annotations\Query;

class UserController
{
    /**
     * @Query()
     * @InjectUser(for="$user")
     * @param User|null $user
     * @return UserType|null
     */
    public function getUser(?User $user): ?UserType
    {
        return $user ? UserTypeBuilder::build($user) : null;
    }
}