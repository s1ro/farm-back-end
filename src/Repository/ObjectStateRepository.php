<?php

namespace App\Repository;

use App\Entity\ObjectState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ObjectState|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectState|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectState[]    findAll()
 * @method ObjectState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectState::class);
    }

    // /**
    //  * @return ObjectState[] Returns an array of ObjectState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectState
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
