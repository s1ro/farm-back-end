<?php

namespace App\Repository;

use App\Entity\GameObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GameObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameObject[]    findAll()
 * @method GameObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameObjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameObject::class);
    }

    // /**
    //  * @return GameObject[] Returns an array of GameObject objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameObject
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
