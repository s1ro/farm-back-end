<?php

namespace App\Repository;

use App\Entity\GameTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GameTime|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameTime|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameTime[]    findAll()
 * @method GameTime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameTimeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameTime::class);
    }

    // /**
    //  * @return GameTime[] Returns an array of GameTime objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameTime
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
