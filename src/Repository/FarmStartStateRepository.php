<?php

namespace App\Repository;

use App\Entity\FarmStartState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FarmStartState|null find($id, $lockMode = null, $lockVersion = null)
 * @method FarmStartState|null findOneBy(array $criteria, array $orderBy = null)
 * @method FarmStartState[]    findAll()
 * @method FarmStartState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FarmStartStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FarmStartState::class);
    }

    // /**
    //  * @return FarmStartState[] Returns an array of FarmStartState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FarmStartState
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
