<?php

namespace App\Repository;

use App\Entity\ObjectSize;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ObjectSize|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectSize|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectSize[]    findAll()
 * @method ObjectSize[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectSizeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectSize::class);
    }

    // /**
    //  * @return ObjectSize[] Returns an array of ObjectSize objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectSize
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
