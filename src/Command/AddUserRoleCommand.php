<?php

namespace App\Command;

use App\Repository\UserRepository;
use App\Service\UserRoleService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AddUserRoleCommand extends Command
{
    protected static $defaultName = 'app:add-user-role';
    protected static $defaultDescription = 'Add user role, avail roles: ';

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        string $name = null
    )
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription . UserRoleService::availableRoles())
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
            ->addArgument('role', InputArgument::REQUIRED, 'Role')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->info('Adding role to user');
        $io->comment('checking user');
        $email = $input->getArgument('email');
        $user = $this->userRepository->findOneBy(['email' => $email]);
        if (!$user) {
            $io->error('User not find!');
            return Command::FAILURE;
        }
        $role = $input->getArgument('role');
        if (!in_array($role, UserRoleService::availableRoles(), true)) {
            $io->error('Invalid role');
            return Command::FAILURE;
        }

        if (in_array($role, $user->getRoles(), true)) {
            $io->error('User already has this role');
            return Command::FAILURE;
        }

        $user->addRole($role);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $io->success('Role added');

        return Command::SUCCESS;
    }
}
