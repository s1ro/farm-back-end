<?php

namespace App\Command;

use App\Entity\FarmStartState;
use App\Entity\GameObject;
use App\Entity\ObjectSize;
use App\Entity\ObjectState;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateStartDataCommand extends Command
{
    protected static $defaultName = 'app:generate-start-data';
    protected static $defaultDescription = 'Generates start data';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        $this->entityManager = $entityManager;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->info('Starting generating command...');
        $this->generateGameObjects($io);
        $this->generateFarmStartState($io);
        $io->success('Generated!');
        return Command::SUCCESS;
    }

    /**
     * @param SymfonyStyle $io
     */
    private function generateGameObjects(SymfonyStyle $io): void
    {
        $io->comment('Generating game objects');
        $io->comment('Generating static objects');
        $this->generateSmallTree($io);
        $this->generateLargeTree($io);
        $io->comment('Generating dynamic objects');
        $this->generateTrough($io);
        $this->generateDrinker($io);
        $this->generateBarn($io);
        $io->comment('Generating animals');
        $this->generateSheep($io);
    }

    /**
     * @param SymfonyStyle $io
     */
    private function generateSmallTree(SymfonyStyle $io): void
    {
        $io->comment('Generating small tree');
        $objectSize = new ObjectSize();
        $objectSize
            ->setYCoord(2)
            ->setXCoord(2);
        $this->entityManager->persist($objectSize);
        $this->entityManager->flush();
        $smallTree = new GameObject();
        $smallTree
            ->setXCoord(1)
            ->setYCoord(1)
            ->setName('smallTree')
            ->setType('smallTree')
            ->setObjectType(GameObject::STATIC_OBJECT)
            ->setSize($objectSize);
        $this->entityManager->persist($smallTree);
        $this->entityManager->flush();
    }

    /**
     * @param SymfonyStyle $io
     */
    private function generateLargeTree(SymfonyStyle $io): void
    {
        $io->comment('Generating large tree');
        $objectSize = new ObjectSize();
        $objectSize
            ->setXCoord(2)
            ->setYCoord(3);
        $this->entityManager->persist($objectSize);
        $this->entityManager->flush();
        $largeTree = new GameObject();
        $largeTree
            ->setXCoord(1)
            ->setYCoord(1)
            ->setName('largeTree')
            ->setType('largeTree')
            ->setObjectType(GameObject::STATIC_OBJECT)
            ->setSize($objectSize);
        $this->entityManager->persist($largeTree);
        $this->entityManager->flush();
    }

    /**
     * @param SymfonyStyle $io
     */
    private function generateTrough(SymfonyStyle $io): void
    {
        $io->comment('Generating trough');
        $objectSize = new ObjectSize();
        $objectSize
            ->setXCoord(3)
            ->setYCoord(1);
        $this->entityManager->persist($objectSize);
        $objectState = new ObjectState();
        $objectState
            ->setName('trough default state')
            ->setImage('asd');
        $this->entityManager->persist($objectState);
        $this->entityManager->flush();
        $trough = new GameObject();
        $trough
            ->setXCoord(1)
            ->setYCoord(1)
            ->setName('trough')
            ->setType('trough')
            ->setSize($objectSize)
            ->setObjectState($objectState)
            ->setObjectType(GameObject::DYNAMIC_OBJECT);
        $this->entityManager->persist($trough);
        $this->entityManager->flush();
    }

    /**
     * @param SymfonyStyle $io
     */
    private function generateDrinker(SymfonyStyle $io): void
    {
        $io->comment('Generating drinker');
        $objectSize = new ObjectSize();
        $objectSize
            ->setXCoord(3)
            ->setYCoord(1);
        $this->entityManager->persist($objectSize);
        $objectState = new ObjectState();
        $objectState
            ->setName('drinker default state')
            ->setImage('asd');
        $this->entityManager->persist($objectState);
        $this->entityManager->flush();
        $drinker = new GameObject();
        $drinker
            ->setXCoord(1)
            ->setYCoord(1)
            ->setName('drinker')
            ->setType('drinker')
            ->setSize($objectSize)
            ->setObjectState($objectState)
            ->setObjectType(GameObject::DYNAMIC_OBJECT);
        $this->entityManager->persist($drinker);
        $this->entityManager->flush();
    }

    /**
     * @param SymfonyStyle $io
     */
    private function generateBarn(SymfonyStyle $io): void
    {
        $io->comment('Generating barn');
        $objectSize = new ObjectSize();
        $objectSize
            ->setXCoord(5)
            ->setYCoord(5);
        $this->entityManager->persist($objectSize);
        $objectState = new ObjectState();
        $objectState
            ->setName('barn default state')
            ->setImage('asd');
        $this->entityManager->persist($objectState);
        $this->entityManager->flush();
        $barn = new GameObject();
        $barn
            ->setXCoord(1)
            ->setYCoord(1)
            ->setName('barn')
            ->setType('barn')
            ->setSize($objectSize)
            ->setObjectState($objectState)
            ->setObjectType(GameObject::DYNAMIC_OBJECT);
        $this->entityManager->persist($barn);
        $this->entityManager->flush();
    }

    /**
     * @param SymfonyStyle $io
     */
    private function generateSheep(SymfonyStyle $io): void
    {
        $io->comment('Generating sheep');
        $objectSize = new ObjectSize();
        $objectSize
            ->setXCoord(1)
            ->setYCoord(1);
        $this->entityManager->persist($objectSize);
        $objectState = new ObjectState();
        $objectState
            ->setName('sheep default state')
            ->setImage('asd');
        $this->entityManager->persist($objectState);
        $this->entityManager->flush();
        $sheep = new GameObject();
        $sheep
            ->setXCoord(1)
            ->setYCoord(1)
            ->setName('sheep')
            ->setType('sheep')
            ->setSize($objectSize)
            ->setObjectState($objectState)
            ->setObjectType(GameObject::ANIMAL)
            ->setWaterPoints(100)
            ->setFoodPoints(100)
            ->setHealthPoints(100);
        $this->entityManager->persist($sheep);
        $this->entityManager->flush();
    }

    /**
     * @param SymfonyStyle $io
     */
    private function generateFarmStartState(SymfonyStyle $io): void
    {
        $io->comment('Generating farm start state');
        $farmStartState = new FarmStartState();
        $farmStartState
            ->setStaticObjectConfig([
                [
                    'type' => 'smallTree',
                    'quantity' => 1
                ],
                [
                    'type' => 'largeTree',
                    'quantity' => 4
                ]
            ])
            ->setDynamicObjectConfig([
                [
                    'type' => 'trough',
                    'quantity' => 1
                ],
                [
                    'type' => 'drinker',
                    'quantity' => 1
                ],
                [
                    'type' => 'barn',
                    'quantity' => 1
                ]
            ])
            ->setAnimalConfig([
                [
                    'type' => 'sheep',
                    'quantity' => 4
                ]
            ]);
        $this->entityManager->persist($farmStartState);
        $this->entityManager->flush();
    }
}
