<?php


namespace App\Security;


use App\GraphQl\Service\User\TokenProvider;
use App\Repository\UserTokenRepository;
use TheCodingMachine\GraphQLite\Security\AuthenticationServiceInterface;

class GraphQlAuthenticator implements AuthenticationServiceInterface
{
    /**
     * @var TokenProvider
     */
    private $tokenProvider;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    public function __construct(TokenProvider $tokenProvider, UserTokenRepository $userTokenRepository)
    {
        $this->tokenProvider = $tokenProvider;
        $this->userTokenRepository = $userTokenRepository;
    }

    /**
     * @inheritDoc
     */
    public function isLogged(): bool
    {
        $user = $this->getUser();
        return isset($user);
    }

    /**
     * @inheritDoc
     */
    public function getUser(): ?object
    {
        $token = $this->tokenProvider->getToken();
        if (!$token) {
            return null;
        }

        $userToken = $this->userTokenRepository->findOneBy(['token' => $token]);
        if (!$userToken) {
            return null;
        }
        return $userToken->getUser();
    }
}