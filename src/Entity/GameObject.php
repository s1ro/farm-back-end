<?php

namespace App\Entity;

use App\Repository\GameObjectRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameObjectRepository::class)
 */
class GameObject
{
    public const STATIC_OBJECT = 'static';
    public const DYNAMIC_OBJECT = 'dynamic';
    public const ANIMAL = 'animal';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $xCoord;

    /**
     * @ORM\Column(type="float")
     */
    private $yCoord;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=ObjectState::class)
     */
    private $objectState;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $healthPoints;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $foodPoints;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $waterPoints;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $objectType;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="gameObjects")
     */
    private $game;

    /**
     * @ORM\OneToOne(targetEntity=ObjectSize::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $size;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getXCoord(): ?float
    {
        return $this->xCoord;
    }

    public function setXCoord(float $xCoord): self
    {
        $this->xCoord = $xCoord;

        return $this;
    }

    public function getYCoord(): ?float
    {
        return $this->yCoord;
    }

    public function setYCoord(float $yCoord): self
    {
        $this->yCoord = $yCoord;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getObjectState(): ?ObjectState
    {
        return $this->objectState;
    }

    public function setObjectState(?ObjectState $objectState): self
    {
        $this->objectState = $objectState;

        return $this;
    }

    public function getHealthPoints(): ?int
    {
        return $this->healthPoints;
    }

    public function setHealthPoints(?int $healthPoints): self
    {
        $this->healthPoints = $healthPoints;

        return $this;
    }

    public function getFoodPoints(): ?int
    {
        return $this->foodPoints;
    }

    public function setFoodPoints(?int $foodPoints): self
    {
        $this->foodPoints = $foodPoints;

        return $this;
    }

    public function getWaterPoints(): ?int
    {
        return $this->waterPoints;
    }

    public function setWaterPoints(?int $waterPoints): self
    {
        $this->waterPoints = $waterPoints;

        return $this;
    }

    public function getObjectType(): ?string
    {
        return $this->objectType;
    }

    public function setObjectType(string $objectType): self
    {
        $this->objectType = $objectType;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getSize(): ?ObjectSize
    {
        return $this->size;
    }

    public function setSize(ObjectSize $size): self
    {
        $this->size = $size;

        return $this;
    }
}
