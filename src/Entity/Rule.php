<?php

namespace App\Entity;

use App\Repository\RuleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RuleRepository::class)
 */
class Rule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rule;

    /**
     * @ORM\ManyToOne(targetEntity=Fact::class, inversedBy="rules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fact;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRule(): ?string
    {
        return $this->rule;
    }

    public function setRule(string $rule): self
    {
        $this->rule = $rule;

        return $this;
    }

    public function getFact(): ?Fact
    {
        return $this->fact;
    }

    public function setFact(?Fact $fact): self
    {
        $this->fact = $fact;

        return $this;
    }
}
