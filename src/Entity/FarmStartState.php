<?php

namespace App\Entity;

use App\Repository\FarmStartStateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FarmStartStateRepository::class)
 */
class FarmStartState
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $staticObjectConfig = [];

    /**
     * @ORM\Column(type="json")
     */
    private $DynamicObjectConfig = [];

    /**
     * @ORM\Column(type="json")
     */
    private $animalConfig = [];

    /**
     * @ORM\OneToMany(targetEntity=Game::class, mappedBy="farmStartState")
     */
    private $games;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStaticObjectConfig(): ?array
    {
        return $this->staticObjectConfig;
    }

    public function setStaticObjectConfig(array $staticObjectConfig): self
    {
        $this->staticObjectConfig = $staticObjectConfig;

        return $this;
    }

    public function getDynamicObjectConfig(): ?array
    {
        return $this->DynamicObjectConfig;
    }

    public function setDynamicObjectConfig(array $DynamicObjectConfig): self
    {
        $this->DynamicObjectConfig = $DynamicObjectConfig;

        return $this;
    }

    public function getAnimalConfig(): ?array
    {
        return $this->animalConfig;
    }

    public function setAnimalConfig(array $animalConfig): self
    {
        $this->animalConfig = $animalConfig;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->setFarmStartState($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->removeElement($game)) {
            // set the owning side to null (unless already changed)
            if ($game->getFarmStartState() === $this) {
                $game->setFarmStartState(null);
            }
        }

        return $this;
    }
}
