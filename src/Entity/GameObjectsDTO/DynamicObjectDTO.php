<?php


namespace App\Entity\GameObjectsDTO;


use App\Entity\Game;
use App\Entity\ObjectSize;
use App\Entity\ObjectState;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 * Class DynamicObjectDTO
 * @package App\Entity\GameObjectsDTO
 */
class DynamicObjectDTO extends StaticObjectDTO
{
    /**
     * @var ObjectState
     */
    private $objectState;

    public function __construct(int $id, string $objectType, int $xCoord, int $yCoord, ObjectSize $size, ?Game $game, string $type, string $name, ObjectState $objectState)
    {
        $this->objectState = $objectState;
        parent::__construct($id, $objectType, $xCoord, $yCoord, $size, $game, $type, $name);
    }

    /**
     * @Field()
     * @return ObjectState
     */
    public function getObjectState(): ObjectState
    {
        return $this->objectState;
    }
}