<?php


namespace App\Entity\GameObjectsDTO;


use App\Entity\GameObject;

class DTOBuilder
{
    /**
     * @param GameObject $gameObject
     * @return StaticObjectDTO
     */
    public static function buildStaticObject(GameObject $gameObject): StaticObjectDTO
    {
        return new StaticObjectDTO(
            $gameObject->getId(),
            $gameObject->getObjectType(),
            $gameObject->getXCoord(),
            $gameObject->getYCoord(),
            $gameObject->getSize(),
            $gameObject->getGame(),
            $gameObject->getType(),
            $gameObject->getName()
        );
    }

    /**
     * @param GameObject[] $gameObjects
     * @return StaticObjectDTO[]
     */
    public static function batchBuildStaticObject(array $gameObjects): array
    {
        $result = [];
        foreach ($gameObjects as $gameObject) {
            $result[] = self::buildStaticObject($gameObject);
        }

        return $result;
    }

    /**
     * @param GameObject $gameObject
     * @return DynamicObjectDTO
     */
    public static function buildDynamicObject(GameObject $gameObject): DynamicObjectDTO
    {
        return new DynamicObjectDTO(
            $gameObject->getId(),
            $gameObject->getObjectType(),
            $gameObject->getXCoord(),
            $gameObject->getYCoord(),
            $gameObject->getSize(),
            $gameObject->getGame(),
            $gameObject->getType(),
            $gameObject->getName(),
            $gameObject->getObjectState()
        );
    }

    /**
     * @param GameObject[] $gameObjects
     * @return DynamicObjectDTO[]
     */
    public static function batchBuildDynamicObject(array $gameObjects): array
    {
        $result = [];
        foreach ($gameObjects as $gameObject) {
            $result[] = self::buildDynamicObject($gameObject);
        }

        return $result;
    }

    /**
     * @param GameObject $gameObject
     * @return AnimalDTO
     */
    public static function buildAnimal(GameObject $gameObject): AnimalDTO
    {
        return new AnimalDTO(
            $gameObject->getId(),
            $gameObject->getObjectType(),
            $gameObject->getXCoord(),
            $gameObject->getYCoord(),
            $gameObject->getSize(),
            $gameObject->getGame(),
            $gameObject->getType(),
            $gameObject->getName(),
            $gameObject->getObjectState(),
            $gameObject->getHealthPoints(),
            $gameObject->getFoodPoints(),
            $gameObject->getWaterPoints()
        );
    }

    /**
     * @param GameObject[] $gameObjects
     * @return AnimalDTO[]
     */
    public static function batchBuildAnimal(array $gameObjects): array
    {
        $result = [];
        foreach ($gameObjects as $gameObject) {
            $result[] = self::buildAnimal($gameObject);
        }

        return $result;
    }
}