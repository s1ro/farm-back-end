<?php


namespace App\Entity\GameObjectsDTO;


use App\Entity\Game;
use App\Entity\ObjectSize;
use App\Entity\ObjectState;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 * Class AnimalDTO
 * @package App\Entity\GameObjectsDTO
 */
class AnimalDTO extends DynamicObjectDTO
{
    /**
     * @var int
     */
    private $healthPoints;

    /**
     * @var int
     */
    private $foodPoints;

    /**
     * @var int
     */
    private $waterPoints;

    public function __construct(
        int $id,
        string $objectType,
        int $xCoord,
        int $yCoord,
        ObjectSize $size,
        ?Game $game,
        string $type,
        string $name,
        ObjectState $objectState,
        int $healthPoints,
        int $foodPoints,
        int $waterPoints
    )
    {
        $this->healthPoints = $healthPoints;
        $this->foodPoints = $foodPoints;
        $this->waterPoints = $waterPoints;
        parent::__construct($id, $objectType, $xCoord, $yCoord, $size, $game, $type, $name, $objectState);
    }

    /**
     * @Field()
     * @return int
     */
    public function getHealthPoints(): int
    {
        return $this->healthPoints;
    }

    /**
     * @Field()
     * @return int
     */
    public function getFoodPoints(): int
    {
        return $this->foodPoints;
    }

    /**
     * @Field()
     * @return int
     */
    public function getWaterPoints(): int
    {
        return $this->waterPoints;
    }
}