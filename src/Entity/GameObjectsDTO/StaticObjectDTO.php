<?php


namespace App\Entity\GameObjectsDTO;


use App\Entity\Game;
use App\Entity\ObjectSize;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 * Class StaticObjectDTO
 * @package App\Entity\GameObjectsDTO
 */
class StaticObjectDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $objectType;

    /**
     * @var float
     */
    private $xCoord;

    /**
     * @var float
     */
    private $yCoord;

    /**
     * @var ObjectSize
     */
    private $size;

    /**
     * @var Game|null
     */
    private $game;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * StaticObjectDTO constructor.
     * @param int $id
     * @param string $objectType
     * @param float $xCoord
     * @param float $yCoord
     * @param ObjectSize $size
     * @param Game|null $game
     * @param string $type
     * @param string $name
     */
    public function __construct(int $id, string $objectType, float $xCoord, float $yCoord, ObjectSize $size, ?Game $game, string $type, string $name)
    {
        $this->id = $id;
        $this->objectType = $objectType;
        $this->xCoord = $xCoord;
        $this->yCoord = $yCoord;
        $this->size = $size;
        $this->game = $game;
        $this->type = $type;
        $this->name = $name;
    }


    /**
     * @Field()
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @Field()
     * @return string
     */
    public function getObjectType(): string
    {
        return $this->objectType;
    }

    /**
     * @Field()
     * @return float
     */
    public function getXCoord(): float
    {
        return $this->xCoord;
    }

    /**
     * @Field()
     * @return float
     */
    public function getYCoord(): float
    {
        return $this->yCoord;
    }

    /**
     * @Field()
     * @return ObjectSize
     */
    public function getSize(): ObjectSize
    {
        return $this->size;
    }

    /**
     * @return Game|null
     */
    public function getGame(): ?Game
    {
        return $this->game;
    }

    /**
     * @Field()
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @Field()
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}