<?php

namespace App\Entity;

use App\Entity\GameObjectsDTO\AnimalDTO;
use App\Entity\GameObjectsDTO\DTOBuilder;
use App\Entity\GameObjectsDTO\DynamicObjectDTO;
use App\Entity\GameObjectsDTO\StaticObjectDTO;
use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=FarmStartState::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $farmStartState;

    /**
     * @ORM\OneToMany(targetEntity=GameObject::class, mappedBy="game")
     */
    private $gameObjects;

    /**
     * @ORM\OneToOne(targetEntity=GameTime::class, mappedBy="game", cascade={"persist", "remove"})
     */
    private $gameTime;

    /**
     * @ORM\OneToMany(targetEntity=Environment::class, mappedBy="game")
     */
    private $environments;

    public function __construct()
    {
        $this->gameObjects = new ArrayCollection();
        $this->environments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFarmStartState(): ?FarmStartState
    {
        return $this->farmStartState;
    }

    public function setFarmStartState(?FarmStartState $farmStartState): self
    {
        $this->farmStartState = $farmStartState;

        return $this;
    }

    /**
     * @return Collection|GameObject[]
     */
    public function getGameObjects(): Collection
    {
        return $this->gameObjects;
    }

    public function addGameObject(GameObject $gameObject): self
    {
        if (!$this->gameObjects->contains($gameObject)) {
            $this->gameObjects[] = $gameObject;
            $gameObject->setGame($this);
        }

        return $this;
    }

    public function removeGameObject(GameObject $gameObject): self
    {
        if ($this->gameObjects->removeElement($gameObject)) {
            // set the owning side to null (unless already changed)
            if ($gameObject->getGame() === $this) {
                $gameObject->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return StaticObjectDTO[]
     */
    public function getStaticObjects(): array
    {
        /** @noinspection NullPointerExceptionInspection */
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('objectType', GameObject::STATIC_OBJECT));

        $objects = $this->gameObjects->matching($criteria);
        return DTOBuilder::batchBuildStaticObject($objects->toArray());
    }

    /**
     * @return DynamicObjectDTO[]
     */
    public function getDynamicObjects(): array
    {
        /** @noinspection NullPointerExceptionInspection */
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('objectType', GameObject::DYNAMIC_OBJECT));

        $objects = $this->gameObjects->matching($criteria);
        return DTOBuilder::batchBuildDynamicObject($objects->toArray());
    }

    /**
     * @return AnimalDTO[]
     */
    public function getAnimals(): array
    {
        /** @noinspection NullPointerExceptionInspection */
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('objectType', GameObject::ANIMAL));

        $objects = $this->gameObjects->matching($criteria);
        return DTOBuilder::batchBuildAnimal($objects->toArray());
    }

    public function getGameTime(): ?GameTime
    {
        return $this->gameTime;
    }

    public function setGameTime(GameTime $gameTime): self
    {
        // set the owning side of the relation if necessary
        if ($gameTime->getGame() !== $this) {
            $gameTime->setGame($this);
        }

        $this->gameTime = $gameTime;

        return $this;
    }

    /**
     * @return Collection|Environment[]
     */
    public function getEnvironments(): Collection
    {
        return $this->environments;
    }

    public function addEnvironment(Environment $environment): self
    {
        if (!$this->environments->contains($environment)) {
            $this->environments[] = $environment;
            $environment->setGame($this);
        }

        return $this;
    }

    public function removeEnvironment(Environment $environment): self
    {
        if ($this->environments->removeElement($environment)) {
            // set the owning side to null (unless already changed)
            if ($environment->getGame() === $this) {
                $environment->setGame(null);
            }
        }

        return $this;
    }
}
