<?php

namespace App\Entity;

use App\Repository\ObjectSizeRepository;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 * @ORM\Entity(repositoryClass=ObjectSizeRepository::class)
 */
class ObjectSize
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $xCoord;

    /**
     * @ORM\Column(type="float")
     */
    private $yCoord;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @Field()
     * @return float|null
     */
    public function getXCoord(): ?float
    {
        return $this->xCoord;
    }

    public function setXCoord(float $xCoord): self
    {
        $this->xCoord = $xCoord;

        return $this;
    }

    /**
     * @Field()
     * @return float|null
     */
    public function getYCoord(): ?float
    {
        return $this->yCoord;
    }

    public function setYCoord(float $yCoord): self
    {
        $this->yCoord = $yCoord;

        return $this;
    }
}
