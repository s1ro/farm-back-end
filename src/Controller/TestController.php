<?php

namespace App\Controller;

use App\Entity\Game;
use App\Repository\AnimalRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/test")
 * Class TestController
 * @package App\Controller
 */
class TestController extends AbstractController
{
    /**
     * @Route("/error", name="test_error", methods={"GET", "POST"})
     * @param LoggerInterface $logger
     * @return Response
     */
    public function testError(LoggerInterface $logger): Response
    {
        $logger->error('Test error!');
        return new JsonResponse(['data' => ['ok' => true]]);
    }

    /**
     * @Route("/ok", name="test_ok")
     * @return Response
     */
    public function testOk(): Response
    {
        return new JsonResponse(['data' => ['ok' => true]]);
    }
}