<?php


namespace App\Controller\BackOffice;


use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 * Class UserController
 * @package App\Controller\BackOffice
 */
class UserController extends AbstractController
{
    /**
     * @Route("/list", name="user_list")
     * @param UserRepository $userRepository
     * @return Response
     */
    public function getUserList(UserRepository $userRepository): Response
    {
        return $this->render('admin/user/user-list.html.twig',[
            'users' => $userRepository->findAll()
        ]);
    }
}