FARM BACK-END
===============
Rename ``` .env.sample ``` in ``` .env ```

To build container with all extensions:
print command in .docker dir
```
docker-compose build
```
To start docker (in __root__ dir)
```
docker-compose -f .docker/docker-compose.yml up -d
```
To install libs __(in ```php``` continer)__:
```
composer install
```
After:   ```composer install```
```
chown www-data:www-data ../tmp/graphqlite*
```
To up migrations use __(in ```php``` container)__:
```
php bin/console doctrine:migrations:migrate
```

