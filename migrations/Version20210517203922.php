<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210517203922 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE object_size (id INT AUTO_INCREMENT NOT NULL, x_coord DOUBLE PRECISION NOT NULL, y_coord DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_object CHANGE size size_id INT NOT NULL');
        $this->addSql('ALTER TABLE game_object ADD CONSTRAINT FK_D5980624498DA827 FOREIGN KEY (size_id) REFERENCES object_size (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D5980624498DA827 ON game_object (size_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game_object DROP FOREIGN KEY FK_D5980624498DA827');
        $this->addSql('DROP TABLE object_size');
        $this->addSql('DROP INDEX UNIQ_D5980624498DA827 ON game_object');
        $this->addSql('ALTER TABLE game_object CHANGE size_id size INT NOT NULL');
    }
}
