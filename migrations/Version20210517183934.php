<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210517183934 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE fact (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, fact VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_6FA45B95B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE farm_start_state (id INT AUTO_INCREMENT NOT NULL, static_object_config JSON NOT NULL, dynamic_object_config JSON NOT NULL, animal_config JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, farm_start_state_id INT NOT NULL, INDEX IDX_232B318CA76ED395 (user_id), INDEX IDX_232B318CB51A9300 (farm_start_state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_object (id INT AUTO_INCREMENT NOT NULL, object_state_id INT DEFAULT NULL, game_id INT DEFAULT NULL, x_coord INT NOT NULL, y_coord INT NOT NULL, size INT NOT NULL, type VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, health_points INT DEFAULT NULL, food_points INT DEFAULT NULL, water_points INT DEFAULT NULL, object_type VARCHAR(255) NOT NULL, INDEX IDX_D5980624D30ED1A9 (object_state_id), INDEX IDX_D5980624E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE object_state (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, image VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rule (id INT AUTO_INCREMENT NOT NULL, fact_id INT NOT NULL, rule VARCHAR(255) NOT NULL, INDEX IDX_46D8ACCC156D5C2A (fact_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(150) DEFAULT NULL, roles JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fact ADD CONSTRAINT FK_6FA45B95B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318CB51A9300 FOREIGN KEY (farm_start_state_id) REFERENCES farm_start_state (id)');
        $this->addSql('ALTER TABLE game_object ADD CONSTRAINT FK_D5980624D30ED1A9 FOREIGN KEY (object_state_id) REFERENCES object_state (id)');
        $this->addSql('ALTER TABLE game_object ADD CONSTRAINT FK_D5980624E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE rule ADD CONSTRAINT FK_46D8ACCC156D5C2A FOREIGN KEY (fact_id) REFERENCES fact (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rule DROP FOREIGN KEY FK_46D8ACCC156D5C2A');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318CB51A9300');
        $this->addSql('ALTER TABLE game_object DROP FOREIGN KEY FK_D5980624E48FD905');
        $this->addSql('ALTER TABLE game_object DROP FOREIGN KEY FK_D5980624D30ED1A9');
        $this->addSql('ALTER TABLE fact DROP FOREIGN KEY FK_6FA45B95B03A8386');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318CA76ED395');
        $this->addSql('DROP TABLE fact');
        $this->addSql('DROP TABLE farm_start_state');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE game_object');
        $this->addSql('DROP TABLE object_state');
        $this->addSql('DROP TABLE rule');
        $this->addSql('DROP TABLE user');
    }
}
